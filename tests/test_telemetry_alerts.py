from src.main import main
from src.models import TelemetryAlertSeverity, TelemetryComponent

def test_telemetry_alerts():
    alerts = main('tests/input/telemetry_data.csv')
    assert len(alerts) == 2

    tstat_alert = alerts[0]
    batt_alert = alerts[1]

    assert tstat_alert.satellite_id == '1000'
    assert tstat_alert.severity == TelemetryAlertSeverity.RED_HIGH
    assert tstat_alert.component == TelemetryComponent.TSTAT

    assert batt_alert.satellite_id == '1000'
    assert batt_alert.severity == TelemetryAlertSeverity.RED_LOW
    assert batt_alert.component == TelemetryComponent.BATT

def test_telemetry_alerts__with_out_of_order_records__should_still_process_correctly():
    expected_alerts = main('tests/input/telemetry_data.csv')
    actual_alerts = main('tests/input/telemetry_data_out_of_order.csv')

    assert len(expected_alerts) == len(actual_alerts)

def test_telemetry_alerts__with_multiple_batt_alerts():
    alerts = main('tests/input/telemetry_data_multiple_batt_alerts.csv')

    assert len(alerts) == 3

    for alert in alerts:
        assert alert.component == TelemetryComponent.BATT
        assert alert.satellite_id == '1000'
        assert alert.severity == TelemetryAlertSeverity.RED_LOW
