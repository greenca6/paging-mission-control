# Paging Mission Control Coding Challenge

This repository contains the solution code to the [paging-mission-control](https://gitlab.com/enlighten-challenge/paging-mission-control) coding challenge.

The solution is written in python, and uses `pipenv` to manage application dependencies and main scripts. While it isn't necessary to use `pipenv`, it is recommended.

To install `pipenv`:

```bash
pip install --user pipenv
```

Then to execute the program, run the following:

```bash
pipenv run main
# or without pipenv:
python -m src.main
```

The output will be written to console as specified in the challenge instructions.

The input file is stored in `input/telemetry_data.csv`. You can modify this file to see how the input changes the output.

To execute tests, run the following:

```bash
pipenv run test
```

# Discussion

One key element left out of the requirements is how the system should handle "stacked" violations that occur within a 5m timeframe. For example, it is not clear how many alerts should be sent/created if there are 10 violations that all happen within a 5 minute window, and the test data given don't illustrate this example.

There are two possible solutions to this ambiguity:

1. Create an alert if there are exactly 3 violations in the 5m time window, **and** you haven't created an alert for this time window yet (ie, avoid duplicate alerts that have the same 5m time reference)
2. Always send a new alert on a new violation record, regardless of prior alerts. This would mean that if there are 10 violations within a 5m window, 8 total alerts would be sent/created (no alerts on the first two violations, then alerts for the following 8 violations).

Both solutions have their upsides and downsides. My solution uses the second approach, as this felt slightly more in line with the requirements, though switching to the first approach would be trivial.
