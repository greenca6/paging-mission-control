import csv
import json

from typing import List
from datetime import datetime as dt ,date

from .models import TelemetryDataRecord, TelemetryComponent
from .satellite_alert_manager import SatelliteAlertManager


DATE_FORMAT = '%Y%m%d %H:%M:%S.%f'

def main(input_file_path='input/telemetry_data.csv'):
    telemetry_data_records: List[TelemetryDataRecord] = []

    # Parse the input file into our array of data records
    with open(input_file_path, 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='|')

        for row in reader:
            timestamp = dt.strptime(row[0], DATE_FORMAT)

            telemetry_data_records.append(
                TelemetryDataRecord(
                    timestamp,
                    row[1],
                    int(row[2]),
                    float(row[3]),
                    float(row[4]),
                    float(row[5]),
                    float(row[6]),
                    TelemetryComponent.from_str(row[7]),
                )
            )

    # Sort the records by timestamp just to be sure we process them in-order
    telemetry_data_records.sort(key=lambda r: r.timestamp)

    alerts = []
    satellite_alert_managers = {}

    # Process the records using the alert managers
    for record in telemetry_data_records:
        # Get/Create the alert manager for the given satellite
        if satellite_alert_managers.get(record.satellite_id) is None:
            satellite_alert_managers[record.satellite_id] = SatelliteAlertManager(record.satellite_id)
        satellite_alert_manager = satellite_alert_managers[record.satellite_id]
        alert = satellite_alert_manager.read_record(record)

        if alert:
            alerts.append(alert)

    return alerts

    

if __name__ == '__main__':
    def serialize(obj):
        if isinstance(obj, (dt, date)):
            return obj.isoformat()
        return str(obj)

    alerts = main()
    print(json.dumps([alert.__dict__ for alert in alerts], indent=4, default=serialize))

