from .telemetry_alert_severity import TelemetryAlertSeverity
from .telemetry_component import TelemetryComponent


class TelemetryAlert:
    def __init__(self, satellite_id: int, severity: TelemetryAlertSeverity, component: TelemetryComponent, timestamp: int) -> None:
        self.satellite_id = satellite_id
        self.severity = severity
        self.component = component
        self.timestamp = timestamp
