from .telemetry_component import TelemetryComponent

class TelemetryDataRecord:
    '''
    Data class representing a single record of telemetry data, read in from our input file.
    '''
    def __init__(self, timestamp: int, satellite_id, red_high_limit: int, yellow_high_limit: int, yellow_low_limit: int, red_low_limit: int, raw_value: int, component: TelemetryComponent) -> None:
        self.timestamp = timestamp
        self.satellite_id = satellite_id
        self.red_high_limit = red_high_limit
        self.yellow_high_limit = yellow_high_limit
        self.yellow_low_limit = yellow_low_limit
        self.red_low_limit = red_low_limit
        self.raw_value = raw_value
        self.component = component
