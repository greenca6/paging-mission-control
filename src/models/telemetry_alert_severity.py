from enum import Enum

class TelemetryAlertSeverity(str, Enum):
    RED_HIGH = 'RED HIGH'
    RED_LOW = 'RED LOW'
