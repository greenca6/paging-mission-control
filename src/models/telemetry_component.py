from enum import Enum

class TelemetryComponent(str, Enum):
    '''
    Component that reports satellite monitoring data, either a battery or thermostat.
    '''
    BATT = 'BATT'
    TSTAT ='TSTAT'

    @staticmethod
    def from_str(name):
        if name.upper() == 'BATT':
            return TelemetryComponent.BATT
        elif name.upper() == 'TSTAT':
            return TelemetryComponent.TSTAT
        
        raise NotImplementedError('Unrecognized component "{name}"')