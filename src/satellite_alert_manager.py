from .models import TelemetryAlert, TelemetryDataRecord, TelemetryComponent, TelemetryAlertSeverity

class SatelliteAlertManager:
    '''
    The Satellite Alert Manager reads in records one by one for one satellite. It knows when violations occur, and whether
    an Alert should be sent based on the requirements.
    '''
    def __init__(self, satellite_id) -> None:
        self.satellite_id = satellite_id
        self.recent_batt_violations = []
        self.recent_tstat_violations = []

    def read_record(self, record: TelemetryDataRecord):
        '''
        Reads in the record and conditionally returns an Alert if there are three
        violation conditions within a 5min window. Assumes that the records are read in-order according to timestamp.

        Requirements aren't clear whether alerts should or should not "stack" (ie what to do when there are
        more than 3 alerts in a 5m window) - so this logic will always send an alert on a new violation as long as there
        are 3 alerts within the past 5m. For example, 5 violations within 5 minutes (1 alert roughly per minute), would result in 3 alerts when
        read by this method one by one.
        '''
        if record.component == TelemetryComponent.BATT:
            return self.__read_batt_record(record)
        elif record.component == TelemetryComponent.TSTAT:
            return self.__read_tstat_record(record)
        
    def __is_within_5_mins(self, timestamp1, timestamp2):
        minutes_apart = (timestamp1 - timestamp2).total_seconds() / 60
        return minutes_apart < 5

    def __read_batt_record(self, record: TelemetryDataRecord):
        is_violation = record.raw_value < record.red_low_limit

        # Push the new violation onto the array, filter out other violations older than 5m from the new one
        if is_violation:
            self.recent_batt_violations.append(record)
            self.recent_batt_violations = list(filter(lambda v: self.__is_within_5_mins(v.timestamp, record.timestamp), self.recent_batt_violations))

            if len(self.recent_batt_violations) >= 3:
                return TelemetryAlert(record.satellite_id, TelemetryAlertSeverity.RED_LOW, TelemetryComponent.BATT, self.recent_batt_violations[0].timestamp)

    def __read_tstat_record(self, record: TelemetryDataRecord):
        is_violation = record.raw_value > record.red_high_limit

        # Push the new violation onto the array, filter out other violations older than 5m from the new one
        if is_violation:
            self.recent_tstat_violations.append(record)
            self.recent_tstat_violations = list(filter(lambda v: self.__is_within_5_mins(v.timestamp, record.timestamp), self.recent_tstat_violations))

            if len(self.recent_tstat_violations) >= 3:
                return TelemetryAlert(record.satellite_id, TelemetryAlertSeverity.RED_HIGH, TelemetryComponent.TSTAT, self.recent_tstat_violations[0].timestamp)
